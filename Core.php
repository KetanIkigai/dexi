<?php
/**
 * Core class to handle loading of class files
 *
 * @class      Core
 * @author     Lavina Chitara
 */
class Core
{
    /**
     * Class Core to autoload classes
     *
     */
    protected $dbConObj;

    function __custruct()
    {
        /* This function would be called automatically */
        spl_autoload_register(function ($class_name) {
            require $class_name.'.php';
        });
    }

    /**
     * To connect with database by using SoapConfig credentials
     *
     * Method   dbconnect
     * Input    void
     * Return   void
     */
    function dbconnect(){
        //create object of class SoapConfig
        $dbConfigObj = new SoapConfig();
        $dbConfig = $dbConfigObj->dbConfigurations;
        $this->dbConObj = new PDO("mysql:host=".$dbConfig['host'].";dbname=".$dbConfig['database'], $dbConfig['username'], $dbConfig['password']);
        $sql = file_get_contents('BrokerBin-1.0.sql');
        $qr = $this->dbConObj->exec($sql);
        return $this->dbConObj;
    }
}
?>