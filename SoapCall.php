<?php
/**
 * SoapCalls to handle all API calls
 *
 * @class      SoapConfig
 * @author     Lavina Chitara
 */
class SoapCall extends Core
{
    protected $_soapurl = '';
    protected $_username = '';
    protected $_password = '';
    protected $_userkey = '';
    protected $_passkey = '';
    protected $_checkoutSec = 0;

    /* Initialize parameters */
    public function __construct(){
        $soapConfigObj = new SoapConfig();
        $soapConfig = $soapConfigObj->configurations;
        $this->_soapurl = $soapConfig['soapurl'];
        $this->_username = $soapConfig['username'];
        $this->_password = $soapConfig['password'];
        $this->_userkey = $soapConfig['userkey'];
        $this->_passkey = $soapConfig['passkey'];
        $this->_checkoutSec = $soapConfig['checkoutsec'];
    }

    /**
     *   Method  authenticate
     *   Use     To get uid to call other service by using uid
     *   Return  uid(string)
     */
    protected function authenticate(){
        $userProtectedKey = $this->_getProtectedKey($this->_userkey, $this->_username);
        $passProtectedKey = $this->_getProtectedKey($this->_passkey, $this->_password);
        $options = array('checkout_seconds' => $this->_checkoutSec);
        return $this->_requestCalls('Authenticate', $options, array('userProtectedKey' => $userProtectedKey, 'passProtectedKey'=> $passProtectedKey));
    }

    /**
     * To handle all API calls
     *
     * Method   _requestCalls
     * Input    $methodName('Authenticate'/'Search')
     *          $options - array - all optional parameters
     *          $params - input parameters - Authenticate - {userProtectedKey/passProtectedKey} Search - {searchText}
     * Return   SoapApi Response {xml/arr/str}
     */
    protected function _requestCalls($methodName, $options, $params){
        try {
            $clientObj = new SoapClient($this->_soapurl);
            switch($methodName){
                case 'Authenticate':
                    return $clientObj->Authenticate($params['userProtectedKey'], $params['passProtectedKey'], $options);
                    break;
                case 'Search':
                    $start_time = microtime(true);
                    $data = $clientObj->Search($params['searchText'], $options);
                    $end_time = microtime(true);
                    $execution_time = round(($end_time - $start_time), 3) . 'seconds.';
                    return array('data'=> $data, 'execution_time' =>  $execution_time);
                    break;
            }
        } catch (Exception $e) {
            return 'Caught exception: '.$e->getMessage();
        }
    }

    /**
     * To update search counter in database
     *  First connect database and after that checks for today's date entry if does not exist then create new entry with counter 1
     *      if exist then update counter by 1
     *
     * Method   _updateCounter
     * Input    void
     * Return   void
     */
    protected function _updateCounter(){
        try {
            //call function of Core class and get database connect object
            $dbConObj = $this->dbconnect();
            $dbConObj->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $stmt = $dbConObj->prepare("SELECT counter FROM searchtrack where searched_date=:today_date");
            $today_date = date('Y-m-d');
            $stmt->bindValue(':today_date', $today_date);
            $stmt->execute();
            $result = $stmt->fetchAll();

            if(empty($result)){
                $stmt = $dbConObj->prepare("INSERT INTO searchtrack (searched_date, counter) VALUES (:today_date,1)");
                $stmt->bindValue(':today_date', $today_date);
            }
            else{
                $stmt = $dbConObj->prepare("UPDATE searchtrack SET counter=:counter WHERE searched_date=:today_date");
                $stmt->bindValue(':counter', $result[0]['counter']+1);
                $stmt->bindValue(':today_date', $today_date);
            }

            $stmt->execute();

        }
        catch(PDOException $e) {
            echo "Error: " . $e->getMessage();
        }

    }

    /**
     * To encrypt username and password with the combination of key (username + userkey) and (password + passkey)
     *
     * Method   _getProtectedKey
     * Input    $key(string), $field(string) -
     * Return   Encrypted key
     */
    protected function _getProtectedKey($key, $field){
        /* Open the cipher */
        $td = mcrypt_module_open (MCRYPT_BLOWFISH, "", MCRYPT_MODE_ECB, "");

        /* Create the IV and determine the keysize length, use MCRYPT_RAND
        * on Windows instead */
        $iv = mcrypt_create_iv(mcrypt_enc_get_iv_size($td), MCRYPT_DEV_RANDOM);
        $ks = mcrypt_enc_get_key_size($td);

        /* Intialize encryption */
        mcrypt_generic_init($td, $key, $iv);

        /* Encrypt data */
        $encrypted = mcrypt_generic($td, $field);

        // CLOSE THE MCRYPT EXTENTION
        mcrypt_module_close($td);

        return base64_encode($encrypted);
    }

    public function getErrorMessage($code){
        switch($code){
            case '-20':
                return "Internal Search Error";
                break;
            default:
                return "Request parameter is wrong";
        }
    }
}
?>