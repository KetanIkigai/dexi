#BrokerBin Search API

To search products by calling BrokerBin API

#Install

First need to create one database and change database settings in the file named SoapConfig.php
for database, (Used to track daily search count)

	public $dbConfigurations = array(
		'host' => 'localhost',
		'username' => 'root',
		'password' => 'root',
		'database' => 'brokerbin'
	);

Setup soap credentials in the same file for calling API

	'soapurl' => 'http://soap.brokerbin.com/brokerbin_search/search.wsdl',
    'username' => 'soapusername',
    'password' => 'soappassword',
    'userkey' => 'key',    //Found from PDF doc
    'passkey' => 'key',    //Shared by client
    'checkoutsec' => 86400

For tracking daily search count, need to setup database.

#Example
For accessing BrokerBin API, need to include file named "BrokenBin.php", and create object of the class as below and pass the search string to the search function:

	require BrokerBin.php;
	$BrokerBinObj = new BrokerBin();
	$BrokerBinObj->search('stringtosearch');

##Other properties:
Default response type is 'Array', that can be changed by resultType property as

	$BrokerBinObj->resultType = 'arr'; //arr/xml/str

Response data gives server information with status code and message, that can be managed by below property

	$BrokerBinObj->statType = 'dbg'; //int/str/dbg

Server information can be removed in reponse data by set value 1 to omitServer property, whereas bydefault it is set to 0

	$BrokerBinObj->omitServer = 1; //to omit server response status information 0/1 default - 0

To remove server information with meta result too use below property

	$BrokerBinObj->omitAll = 0;

To sort result set on the basis of different property

	$BrokerBinObj->sortBy = 'company'; //country/mfg/qty
	$BrokerBinObj->sortOrder = 'DESC'; //ASC/DESC	default - ASC

To set max result set limit and offset,

	$BrokerBinObj->offset = 0;
	$BrokerBinObj->maxResultset = 50;

SearchType allow to search string in part separated by space,

	$BrokerBinObj->searchType = 'partkey';
	$BrokerBinObj->searchs = 2; //max limit of parts is 10


##Output
  Below property is used to get searched result with server configuration

	$BrokerBinObj->searchResult

  and,
	API search result, also give api excecution time in result

    $BrokerBinObj->searchResult->api_execution_time

	If there is any error, can get error message by below property

	$BrokerBinObj->responseMessage
    
#Release History
 - 1
	 - Allow to search string
	 - Allow to change default properties
 - 2
	 - Setup database to track daily searches
	 

