CREATE TABLE IF NOT EXISTS `searchtrack` (
  `id` INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  `searched_date` DATE NOT NULL,
  `counter` INTEGER NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE(`searched_date`)
) ENGINE=MyISAM;