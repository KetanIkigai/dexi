<?php
/**
 * BrokerBin to handle requests from view
 *
 * @class      BrokerBin
 * @author     Lavina Chitara
 */
class BrokerBin extends SoapCall
{
	protected $_uid = '';
	public $searchResult = '';
	public $responseMessage	= '';
	public $resultType = 'arr';
	public $statType = 'dbg';
	public $omitServer = 0;
	public $searchType = 'partkey';
	public $sortBy = 'company';
	public $sortOrder = 'ASC';
	public $offset = 0;
	public $maxResultset = 50;
	public $omitAll = 0;
	public $searches = 1;
	public $products = '';

	/**
	 *   Method  __construct
	 *   Use     Constructor that is called automatically when object of class is created
	 * 			 It checks for uid if it is not set then call a method to get uid
	 *   Input	 void
	 *   Return  void
	 */
	public function __construct(){
		parent::__construct();

		if(!$this->_uid)
			$this->_authenticate();

	}

	/**
	 *   Method  search
	 *   Use     To get searched products, it sets product and searchResult array with server data/meta_result/resultset
	 * 			 After setting all parameters it calls SoapCall::_requestCalls() by sending parameters
	 *   Input	 $searchText(string)
	 *   Return  void
	 */
	public function search($searchText){
		if($this->_uid != '')
		{
			if(str_word_count($searchText) > 10 && $this->searches > 10){
				$this->responseMessage = 'More than 10 parts are not allowed!!! Search result would consider only 10 parts of entered search';
				$this->searches = 10;
			}

			$options['uid'] = $this->_uid;
			$options['result_type'] = $this->resultType;
			$options['stat_type'] = $this->statType;
			$options['omit_server'] = $this->omitServer;
			$options['search_type'] = $this->searchType;
			$options['sort_by'] = $this->sortBy;
			$options['sort_order'] = $this->sortOrder;
			$options['offset'] = $this->offset;
			$options['max_resultset'] = $this->maxResultset;
			$options['omit_all'] = $this->omitAll;
			$options['searches'] = $this->searches;

			$searchedResultData = $this->_requestCalls('Search', $options, array('searchText' => $searchText));
			$searchedData = $searchedResultData['data'];
			if($searchedData == 'Internal Search Error') {
				$this->responseMessage = $searchedData . ': Please check parameters.';
			}else if(is_int($searchedData)){
				$this->responseMessage = $searchedData . ': ' . $this->getErrorMessage($searchedData);
			}elseif($searchedData != ''){

				//update search counter only if search gives any result
				$this->_updateCounter();

				if($this->resultType == 'xml'){
					$simpleXml = simplexml_load_string($searchedData);
					$jsonData = json_encode($simpleXml->resultset);
					$searchedData = json_decode($jsonData);
					$searchedData->api_execution_time = $searchedResultData['execution_time'];
					$this->searchResult = $searchedData;
					$this->products = $this->searchResult->result;
				}
				elseif($this->resultType == 'str'){
					$searchedData = unserialize($searchedData);
					$searchedData['api_execution_time'] =  $searchedResultData['execution_time'];
					$this->searchResult = $searchedData;
					$this->products = $this->searchResult['resultset'];
				}
				else{
					$searchedData['api_execution_time'] =  $searchedResultData['execution_time'];
					$this->searchResult = $searchedData;
					$this->products = $this->searchResult['resultset'];
				}
			}
			else
				$this->responseMessage = 'No record found';
		}
	}

	/**
	 *   Method  _authenticate
	 *   Use     To handle authentication that will call method of class SoapCall::authenticate to generate and set uid (authentication key)
	 *   Input	 void
	 *   Return  void
	 */
	protected function _authenticate(){
		$this->_uid = $this->authenticate();
		if($this->_uid == 'Failed Authentication'){
			$this->responseMessage = $this->_uid;
			$this->_uid = '';
		}
	}
}
?>