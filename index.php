<!DOCTYPE html>
<html>
<head>

</head>
<body>
<div >
    <!-- to show filter options -->
    <div style="width:50%; float:left;">
        BrokerBin
        <Form>
            <input type="text" name="q" placeholder="Enter your search string" value="<?php echo isset($_GET['q'])?$_GET['q']:''; ?>" />
            <h3>{GLOBALS}</h3>
            <div>
                result_type<br/>
                <input type="radio" name="return_type" value="xml" <?php echo isset($_GET['return_type']) && $_GET['return_type'] == 'xml'?'checked':''; ?>/>xml
                <input type="radio" name="return_type" value="arr" <?php echo !isset($_GET['return_type'])?'checked':'' ?> <?php echo isset($_GET['return_type']) && $_GET['return_type'] == 'arr'?'checked':''; ?>/>arr
                <input type="radio" name="return_type" value="str" <?php echo isset($_GET['return_type']) && $_GET['return_type'] == 'str'?'checked':''; ?> />str
            </div>

            <div>
                stat_type<br/>
                <input type="radio" name="stat_type" value="int" <?php echo isset($_GET['stat_type']) && $_GET['stat_type'] == 'int'?'checked':''; ?> />int
                <input type="radio" name="stat_type" value="str" <?php echo !isset($_GET['stat_type'])?'checked':'' ?> <?php echo isset($_GET['stat_type']) && $_GET['stat_type'] == 'str'?'checked':''; ?> />str
                <input type="radio" name="stat_type" value="dbg" <?php echo isset($_GET['stat_type']) && $_GET['stat_type'] == 'dbg'?'checked':''; ?> />dbg
            </div>

            <div>
                <input type="checkbox" name="chk_omit_server" value="1" <?php echo isset($_GET['chk_omit_server']) && $_GET['chk_omit_server'] == 1?'checked':''; ?>  />omit server
            </div>

            <h3>{OTHER OPTS}</h3>

            <div>
                search_type<br/>
                <input type="radio" name="search_type" value="keyword" <?php echo !isset($_GET['search_type'])?'checked':'' ?> <?php echo isset($_GET['search_type']) && $_GET['search_type'] == 'keyword'?'checked':''; ?> />Keyword
                <input type="radio" name="search_type" value="part" <?php echo isset($_GET['search_type']) && $_GET['search_type'] == 'part'?'checked':''; ?>  />Part
            </div>
            <div>
                searches
                <input type="textbox" name="searches" value="<?php echo isset($_GET['searches']) && $_GET['searches'] != ''?$_GET['searches']:'1' ?>"/>
            </div>
            <div>
                sort_order<br/>
                <input type="radio" name="sort_order" value="ASC" <?php echo !isset($_GET['sort_order'])?'checked':'' ?> <?php echo isset($_GET['sort_order']) && $_GET['sort_order'] == 'ASC'?'checked':''; ?> />ASC
                <input type="radio" name="sort_order" value="DESC" <?php echo isset($_GET['sort_order']) && $_GET['sort_order'] == 'DESC'?'checked':''; ?>  />DESC
            </div>

            <div>
                sortBy<br/>
                <input type="radio" name="sort_by" value="company" <?php echo !isset($_GET['sort_by'])?'checked':'' ?> <?php echo isset($_GET['sort_by']) && $_GET['sort_by'] == 'company'?'checked':''; ?> />company
                <input type="radio" name="sort_by" value="country" <?php echo isset($_GET['sort_by']) && $_GET['sort_by'] == 'country'?'checked':''; ?> />country
                <input type="radio" name="sort_by" value="mfg" <?php echo isset($_GET['sort_by']) && $_GET['sort_by'] == 'mfg'?'checked':''; ?> />mfg
                <input type="radio" name="sort_by" value="qty" <?php echo isset($_GET['sort_by']) && $_GET['sort_by'] == 'qty'?'checked':''; ?> />qty
            </div>

            <div>
                Max Result set
                <input type="textbox" name="max_result_set" value="<?php echo isset($_GET['max_result_set']) && $_GET['max_result_set'] != ''?$_GET['max_result_set']:'50' ?>"/>
                <br/>
                offset
                <input type="textbox" name="offset" value="<?php echo isset($_GET['offset']) && $_GET['offset'] != ''?$_GET['offset']:'0' ?>"/>
                <br/>
                <input type="checkbox" name="omit_all" value="1" <?php echo isset($_GET['omit_all']) && $_GET['omit_all'] == 1?'checked':''; ?>  />omit all
            </div>

            <input type="submit" />
        </Form>
    </div>

    <!--to show result-->
    <div style="width:50%; float:right;">
        <?php
        if(!empty($_GET))
        {
            if($_GET['q'] == ''){
                echo "Please enter your search text";
            }
            else{
                /**
                 * To list out all products
                 *
                 * Main Index file
                 * @author  Lavina Chitara
                 */

                /* To automatically load files */
                spl_autoload_register(function ($class_name) {
                    require $class_name.'.php';
                });

                /*
                 * To get all products
                 */

                $BrokerBinObj = new BrokerBin();

                //Global parameters
                if(isset($_GET['return_type']) && $_GET['return_type'] != ''){
                    $BrokerBinObj->resultType = $_GET['return_type']; //arr/xml/str
                }

                if(isset($_GET['stat_type']) && $_GET['stat_type'] != ''){
                    $BrokerBinObj->statType = $_GET['stat_type']; //int/str/dbg
                }

                if(isset($_GET['chk_omit_server']) && $_GET['chk_omit_server'] != '') {
                    $BrokerBinObj->omitServer = $_GET['chk_omit_server']; //to omit server response status code
                }

                //Optional parameters
                if(isset($_GET['search_type']) && $_GET['search_type'] != '') {
                    $BrokerBinObj->searchType = $_GET['search_type'];
                }
                $BrokerBinObj->searchType = 'part'; //part or keyword

                if(isset($_GET['sort_by']) && $_GET['sort_by'] != '') {
                    $BrokerBinObj->sortBy = $_GET['sort_by'];
                }

                if(isset($_GET['sort_order']) && $_GET['sort_order'] != '') {
                    $BrokerBinObj->sortOrder = $_GET['sort_order'];
                }

                if(isset($_GET['offset']) && $_GET['offset'] != '') {
                    $BrokerBinObj->offset = $_GET['offset'];
                }

                if(isset($_GET['max_result_set']) && $_GET['max_result_set'] != '') {
                    $BrokerBinObj->maxResultset = $_GET['max_result_set'];
                }

                if(isset($_GET['omit_all']) && $_GET['omit_all'] != '') {
                    $BrokerBinObj->omitAll = $_GET['omit_all'];
                }

                if(isset($_GET['searches']) && $_GET['searches'] != '') {
                    $BrokerBinObj->searches = $_GET['searches'];
                }

                //$BrokerBinObj->searches = 10;

                $BrokerBinObj->search($_GET['q']);

                /**
                 * Getting search string
                 */


                if($BrokerBinObj->responseMessage != '')
                    echo $BrokerBinObj->responseMessage;
                ?>
                <?php if($BrokerBinObj->searchResult != ''):?>
                    <p>Search results for <strong> <?php echo $_GET['q']; ?></strong></p>
                    <?php echo '<pre>'; print_r($BrokerBinObj->searchResult); exit; ?>
                    <?php
                    //Showing all products
                    foreach($BrokerBinObj->products as $productArr){
                        foreach($productArr as $product){
                            if($BrokerBinObj->resultType == 'xml'){
                                echo $product->company;
                            }
                            else{
                                echo $product['company'];
                            }
                            echo '<br/>';
                        }
                        echo '<br/>';
                    }
                    ?>
                <?php endif; ?>
            <?php
            }
        }
        ?>
    </div>


</div>
</body>
</html>

